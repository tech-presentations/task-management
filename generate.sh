#!/usr/bin/env bash

set -x
set -e

cd public

wget https://github.com/hakimel/reveal.js/archive/master.tar.gz
tar -xzvf master.tar.gz
mv reveal.js-master reveal.js

pandoc -t revealjs -s ../README.md -o index.html -V revealjs-url=./reveal.js -V theme=solarized
