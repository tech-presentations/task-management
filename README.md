---
author: Drew Stinnett
title: Productivity and Task Management
data-background: 4x3_ppt_4.png

---
## Productivity and Task Management

## Using tools like [TaskWarrior](https://taskwarrior.org/)

## Principal Terms

* **Tasks**: Smallest unit of measurable progress on a project
* **Recurring Tasks**: Things that need to be done weekly/daily/monthly/etc
* **Interrupt Tasks**: Tasks which are not part of a larger project, and often get completed before you add them to your `TODO`
* **Projects**: Contains many tasks
* **Technical Debt**: Accumulation of lower priority tasks
* **Classifications**: Arbitrary tags on how to group a task or project

::: notes

This is my note.

- It can contain Markdown
- like this list

:::

## Tasks

Generally something you can get done in a single session at your laptop

## Examples:

- Install package tmux on a set of servers
- Research using Docker containers in Ansible
- Try out PowerShell on your mac

## Recurring Tasks

Uh…tasks that recur

### Examples:

- Ensure Tickets are up to date `daily`
- Check VIP server logins `daily`
- Review expired accounts `daily`
- Review ‘fail-open’ duo logins `weekly`

## Interrupt Tasks

- Consume your time
- Force a context switch in your workflow
- Often become under appreciated as they are not part of a formal project
- Worthy of being tracked, just like non-interrupt tasks
- Part of the Job

- Interrupt driven work == Accomplishments without a documented task
- Fix this by ‘logging’ interrupt tasks

### Examples:
- Joe walks by for advice on Firewall configurations
- Mary needs a quick package installed on a host
- You need to consult your boss on project priorities

## Projects

Multiple tasks come together to form a higher ‘Project’

### Examples:

#### Project: Satellite v6 Upgrade
- Task: Build test server for Satellite v6
- Task: Install Satellite Package
- Task: Hook up test client to Satellite host
- Etc…

## Classifications

'Classify' your tasks for easier sorting and reporting later

Tasks and projects can have multiple classifications

## Examples:
- Helped Henry with some ansible concepts.  Classification: mentoring
- Wrote script to detect hacked drupal site. Classification: security
- Deployed Satellite v6. Classification: yearly-review, Classification: linux

## Technical Debt

- Low priority tasks with undefined due dates
- Low priority doesn’t mean they should never be done

### Examples:

- Report on renamed package names in puppet (python-pip vs python2-pip)

## Why TaskWarrior?

- Command Line Interface
- We’re already in the CLI
- No additional tool to open
- No website to visit
- Easy to script around
- Free and OpenSource

---

TODO: Edit vs Shortcuts

---

## Installing TaskWarrior

- OSX: `brew install task`
- Linux: `apt install taskwarrior` or `yum install task`
- Windows: Check out [install doc](https://taskwarrior.org/download/)

